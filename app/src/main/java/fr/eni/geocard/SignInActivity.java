package fr.eni.geocard;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import fr.eni.geocard.services.SignInService;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, SignInService.SignInListeners {
    private static final int CODE_SIGN_IN = 1;
    private static final String TAG = "SignInActivity";
    private SignInService signInService;
    private ServiceConnection serviceConnection;
    private GoogleSignInClient googleSignInClient;
    private static String EXTRA_LOGIN_SUCCESS = "loginSucessfull";
    private boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        findViewById(R.id.sign_in_button).setOnClickListener(this);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                signInService = ((SignInService.SignInServiceBinder)service).getService(SignInActivity.this);
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
                signInService = null;
            }
        };
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        googleSignInClient = GoogleSignIn.getClient(getBaseContext(), gso);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                this.signIn();
                break;
        }
    }

    public void signIn() {
        Intent intent = new Intent(SignInActivity.this, SignInService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, CODE_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == CODE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            signInService.handleSignInResult(task);
        }
    }

    @Override
    public void onSign(boolean hasError) {
        if (hasError){
            alertWrongUser();
        }else {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_LOGIN_SUCCESS, !hasError);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void alertWrongUser(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.auth_error)
                .setNegativeButton(R.string.alert_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
    }

    @Override
    public void onStop() {
        if (mBound) {
            try {
                unbindService(serviceConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        if (!mBound){
            signIn();
        }
        super.onResume();
    }
}
