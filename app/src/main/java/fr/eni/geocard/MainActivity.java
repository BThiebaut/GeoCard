package fr.eni.geocard;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.osmdroid.views.overlay.Marker;

import fr.eni.geocard.bo.Media;
import fr.eni.geocard.bo.PostCard;
import java.util.ArrayList;
import java.util.List;

import fr.eni.geocard.Repository.PostCardRepository;
import fr.eni.geocard.adapters.PortraitCardAdapter;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.bo.User;
import fr.eni.geocard.fragments.ListFragment;
import fr.eni.geocard.fragments.MapFragment;
import fr.eni.geocard.services.LocalisationService;
import fr.eni.geocard.services.MediaService;
import fr.eni.geocard.services.PostCardService;
import fr.eni.geocard.services.SignInService;
import fr.eni.geocard.services.UserService;
import fr.eni.geocard.utils.Utils;

public class MainActivity extends AppCompatActivity implements MapFragment.OnFragmentMapListener {

    public static final int REQUEST_CODE_LOGIN = 1;
    private static final int REQUEST_CODE_ADD = 2;
    private static final int REQUEST_CODE_PERM = 3;
    private static final String TAG = "MainActivity";
    private static double MAX_DISTANCE = 1d;
    private static boolean RECREATED;

    private LocalisationService localisationService;
    private boolean mBound;
    private FloatingActionButton addBtn;
    private List<PostCard> listPostCards;
    private ListView listCards;
    private String inVisu;

    private PostCard selectedPostcard;
    public static String EXTRA_SLCT_POSTCARD = "selectedPostcard";

    static {
        RECREATED = false;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBound = true;
            localisationService = ((LocalisationService.LocalisationServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            localisationService = null;
        }
    };

    private void startLocalisationService() {
        Intent intentPlayer = new Intent(this, LocalisationService.class);
        bindService(intentPlayer, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            MenuItem item = menu.findItem(R.id.menu_main_list_cards);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_list_cards:
                ListView liste = findViewById(R.id.main_list_cards);
                if (liste.getVisibility() == View.INVISIBLE) {
                    liste.setVisibility(View.VISIBLE);
                } else {
                    liste.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.menu_btn_sync:
                requestSync();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startLocalisationService();
        addBtn = findViewById(R.id.btn_add_card);
        addBtn.setOnClickListener(l -> {
            addPostCard();
        });
        Utils.requestPermissions(this, REQUEST_CODE_PERM);
        listPostCards = new ArrayList<>();
    }

    @Override
    public void onStop() {
        if (mBound) {
            try {
                unbindService(serviceConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onStop();
    }



    @Override
    public void onResume() {
        if (!mBound) {
            startLocalisationService();
        }
        actualisePosition();
        populateListClosestCards();
        super.onResume();
    }

    private void actualisePosition() {
        MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.frag_map);
        if (mapFragment != null && mapFragment.isAdded()){
            mapFragment.updateMapPosition();
        }
    }

    private void populateListClosestCards() {
        PostCardService postCardService = PostCardService.getInstance();
        MediaService mediaService = MediaService.getInstance();

        postCardService.getAllByDistance(getApplication(), MAX_DISTANCE, this, new PostCardService.postCardListener() {
            @Override
            public void onGetAllResponse(List<PostCard> postCards) {
                listPostCards.clear();
                listPostCards.addAll(postCards);

                for (PostCard postcard : postCards) {
                    UserService.getInstance().getById(postcard.getUserId(), getApplication(), MainActivity.this, user -> {
                        postcard.setUser(user);
                        mediaService.getByCard(postcard.getId(), getApplication(), MainActivity.this, media -> {
                            for (Media m : media) {
                                postcard.getMedias().add(m);
                            }
                            PortraitCardAdapter a = (PortraitCardAdapter) listCards.getAdapter();
                            a.notifyDataSetChanged();
                            //postcard.setMedias(media);
                        });
                    });
                }

                if (listCards == null || listCards.getAdapter() == null) {
                    PortraitCardAdapter portraitCardAdapter = new PortraitCardAdapter(MainActivity.this, listPostCards, getApplication());
                    listCards = findViewById(R.id.main_list_cards);
                    listCards.setAdapter(portraitCardAdapter);

                    listCards.setOnItemClickListener((parent, view, position, id) -> {
                        selectedPostcard = (PostCard) parent.getAdapter().getItem(position);
                        visualizePostcard();
                    });
                }
            }

            @Override
            public void onGetOneResponse(PostCard postCard) {

            }
        });
    }

    // TODO
    public void addPostCard() {
        if (SignInService.appUser != null) {
            Intent intent = new Intent(this, addCardActivity.class);
            startActivityForResult(intent, REQUEST_CODE_ADD);
        } else {
            requestLogin();
        }
    }

    public void requestLogin() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_LOGIN) {
                addPostCard();
            }
            if (requestCode == REQUEST_CODE_ADD){
                onMapReady();
            }
        }
    }

    @Override
    public void onMapReady() {
        MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.frag_map);
        if (mapFragment != null && mapFragment.isAdded()){

            PostCardService.getInstance().getAllByDistance(MainActivity.this.getApplication(), MAX_DISTANCE, this, new PostCardService.postCardListener() {
                @Override
                public void onGetAllResponse(List<PostCard> postCards) {
                    mapFragment.resetMarker();
                    for (PostCard pc : postCards) {
                        UserService.getInstance().getById(pc.getUserId(), getApplication(), MainActivity.this, user -> {
                            pc.setUser(user);
                            mapFragment.addMarquer(pc);
                        });
                    }
                }
                @Override
                public void onGetOneResponse(PostCard postCard) {}
            });
        }
    }

    @Override
    public void onMarkerTap(String uuid) {
        // TODO récupérer la postcard avec l'uuid fournis et afficher la visualisation
        if (inVisu == null || "".equals(inVisu)){
            inVisu = uuid;

            PostCardService.getInstance().getById(uuid, getApplication(), this, new PostCardService.postCardListener() {
                @Override
                public void onGetAllResponse(List<PostCard> postCards) {}

                @Override
                public void onGetOneResponse(PostCard postCard) {
                    UserService.getInstance().getById(postCard.getUserId(), getApplication(), MainActivity.this, user -> {
                        postCard.setUser(user);
                        selectedPostcard = postCard;
                        visualizePostcard();
                    });
                }
            });
        }
    }

    @Override
    public void onMapUpdate() {
        onMapReady();
        populateListClosestCards();
    }

    @Override
    public void onChangeMaxDistance(double max) {
        MAX_DISTANCE = max;
        onMapUpdate();
    }

    public void requestSync(){
        if (localisationService != null){
            actualisePosition();
            onMapUpdate();
        }
    }

    private void visualizePostcard() {
        UserService.getInstance().getById(selectedPostcard.getUserId(), getApplication(), MainActivity.this, user -> {
            selectedPostcard.setUser(user);
            MediaService mediaService = MediaService.getInstance();
            mediaService.getByCard(selectedPostcard.getId(), getApplication(), MainActivity.this, media -> {
                for (Media m : media) {
                    selectedPostcard.getMedias().add(m);
                }
                Intent intent = new Intent(MainActivity.this, VisualisationActivity.class);
                intent.putExtra(EXTRA_SLCT_POSTCARD, selectedPostcard);
                inVisu = null;
                startActivity(intent);
            });
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allOk = true;
        for (int res : grantResults){
            if (allOk && res != RESULT_OK){
                allOk = false;
            }
        }
        if (allOk){
            if (!RECREATED){
                Handler handler = new Handler();
                Activity ctx = this;
                handler.postDelayed(() -> {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
                    {
                        ctx.finish();
                        ctx.startActivity(ctx.getIntent());
                    } else ctx.recreate();
                    RECREATED = true;
                }, 1);
            }
        }else {
            Utils.requestPermissions(this, REQUEST_CODE_PERM);
        }
    }
}
