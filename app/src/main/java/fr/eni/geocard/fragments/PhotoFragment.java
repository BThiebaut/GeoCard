package fr.eni.geocard.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.eni.geocard.R;
import fr.eni.geocard.utils.FileUtils;

import static android.app.Activity.RESULT_OK;

public class PhotoFragment extends Fragment {

    private static final int PICK_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int REQUEST_TAKE_PHOTO = 3;
    private static final int PERMISSION_CAMERA = 4;
    private OnFragmentPhotoListener mListener;
    private ImageView imgPreview;
    private TextView txtPreview;
    private Button btnTakePhoto;
    private Uri selectedImage;
    private String selectedImagePath;
    private boolean permissionsReady;

    public static final String INPUT_IMAGE = "image";


    public PhotoFragment() {
        // Required empty public constructor
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CAMERA){
            permissionsReady=true;
            takePhoto();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        imgPreview = view.findViewById(R.id.image_preview);
        txtPreview = view.findViewById(R.id.txt_select_galerie);
        btnTakePhoto = view.findViewById(R.id.btn_take_photo);

        imgPreview.setOnClickListener(i -> {
            selectImageGalerie();
        });

        btnTakePhoto.setOnClickListener(i -> {
            takePhoto();
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentPhotoListener) {
            mListener = (OnFragmentPhotoListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void selectImageGalerie(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                selectedImage = FileProvider.getUriForFile(getContext(),
                        "fr.eni.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch(requestCode){
                case PICK_IMAGE:
                    selectedImage = data.getData();
                    imgPreview.setImageURI(selectedImage);
                    String path = FileUtils.getPath(getContext(), data.getData());
                    selectedImagePath = path;
                    break;
                case REQUEST_TAKE_PHOTO :
                    if (selectedImage != null){
                        imgPreview.setImageURI(selectedImage);
                        String pathLeChien = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + selectedImage.getPathSegments().get(selectedImage.getPathSegments().size() - 1);
                        selectedImagePath = pathLeChien;
                    }
                    break;
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

    private void takePhoto(){
        if (!permissionsReady){
            checkPersmissions();
        }else {
            dispatchTakePictureIntent();
            /*
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
            */
        }
    }

    public String getSelectedPhoto(){
        return selectedImagePath != null && !"".equals(selectedImagePath) ? selectedImagePath : null;
    }


    public interface OnFragmentPhotoListener {
    }

    void checkPersmissions(){
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{ android.Manifest.permission.CAMERA},
                        PERMISSION_CAMERA);
            }
        }else
            permissionsReady = true;

        dispatchTakePictureIntent();
    }


}
