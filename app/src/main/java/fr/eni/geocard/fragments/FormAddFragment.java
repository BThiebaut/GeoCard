package fr.eni.geocard.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import fr.eni.geocard.R;
import fr.eni.geocard.bo.Media;
import fr.eni.geocard.services.ApiService;
import fr.eni.geocard.services.LocalisationService;
import fr.eni.geocard.services.PostCardService;

public class FormAddFragment extends Fragment implements TextWatcher, ApiService.ApiServiceListeners {

    private static final String INPUT_TITLE = "input_title";
    private EditText editTitle;
    private EditText searchMusic;
    private Spinner spinnerMusic;
    private EditText searchVideo;
    private Spinner spinnerVideo;
    private ArrayAdapter<Media> spinnerAdapter;
    private ArrayAdapter<Media> spinnerAdapterVideo;
    private List<Media> adapterListe;
    private List<Media> adapterListeVideo;
    private OnFragmentFormAddListener mListener;
    private double latitude;
    private double longitude;
    private ApiService apiService;


    public FormAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_add, container, false);
        editTitle = view.findViewById(R.id.form_add_input_title);
        if (LocalisationService.lastLocation != null){
            latitude = LocalisationService.lastLocation.getLatitude();
            longitude = LocalisationService.lastLocation.getLongitude();
        }

        searchMusic = view.findViewById(R.id.form_add_input_music);
        spinnerMusic = view.findViewById(R.id.form_add_spinner_music);
        /*
        searchVideo = view.findViewById(R.id.form_add_input_video);
        spinnerVideo = view.findViewById(R.id.form_add_spinner_video);
        */
        searchMusic.addTextChangedListener(this);
        //searchVideo.addTextChangedListener(this);

        apiService = new ApiService(this, getContext());

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentFormAddListener) {
            mListener = (OnFragmentFormAddListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {

        String name = s.toString();
        View vew = getActivity().getCurrentFocus();


        if (name.length() > 2){
            if (apiService != null){
                apiService.searchFor(name);
            }
        }else if (name.length() == 0){
            if (adapterListe != null && spinnerAdapter != null){
                adapterListe.clear();
                spinnerAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onSearchResult(List<Media> musics) {
        loadSpinner(musics);
    }


    @Override
    public void onVideoResult(List<Media> videos) {
        loadSpinnerVideo(videos);
    }


    protected void loadSpinner(List<Media> musics){
        if (spinnerAdapter == null){
            adapterListe = new ArrayList<>();
            adapterListe.addAll(musics);
            spinnerAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, adapterListe);
            spinnerMusic.setAdapter(spinnerAdapter);
        }else {
            adapterListe.clear();
            adapterListe.addAll(musics);
            spinnerAdapter.notifyDataSetChanged();
        }
    }

    protected void loadSpinnerVideo(List<Media> videos){
        if (spinnerAdapter == null){
            adapterListeVideo = new ArrayList<>();
            adapterListeVideo.addAll(videos);
            spinnerAdapterVideo = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, adapterListeVideo);
            spinnerVideo.setAdapter(spinnerAdapterVideo);
        }else {
            adapterListeVideo.clear();
            adapterListeVideo.addAll(videos);
            spinnerAdapterVideo.notifyDataSetChanged();
        }
    }

    public interface OnFragmentFormAddListener {
        void onSubmitFormAdd();
    }

    public Dictionary<String, Object> getFormDatas(){
        Dictionary<String, Object> results = new Hashtable<>();
        results.put(PostCardService.VALUE_TITRE, editTitle.getText().toString());
        results.put(PostCardService.VALUE_LATITUDE, latitude);
        results.put(PostCardService.VALUE_LONGITUDE, longitude);
        if (adapterListe != null && adapterListe.size() > 0){
            results.put(PostCardService.VALUE_MUSIC, spinnerMusic.getSelectedItem());
        }

        return results;
    }
}
