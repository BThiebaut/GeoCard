package fr.eni.geocard.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fr.eni.geocard.R;
import fr.eni.geocard.services.PlayerService;

public class PlayerFragment extends Fragment {

    private TextView plTitle;
    private Button plToggle;
    private PlayerService playerService;
    private String url;
    private Drawable icPlay;
    private Drawable icPause;


    public PlayerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, container, false);

        plTitle = view.findViewById(R.id.player_title);
        plToggle = view.findViewById(R.id.player_toggle);
        icPlay = ResourcesCompat.getDrawable(getResources(), android.R.drawable.ic_media_play, null);
        icPause = ResourcesCompat.getDrawable(getResources(), android.R.drawable.ic_media_pause, null);
        playerService = new PlayerService();

        plToggle.setOnClickListener(l -> {
            try{
                if (playerService != null && url != null && !"".equals(url)){
                    if (playerService.isPlaying()){
                        stop();
                    }else {
                        play();
                    }
                }
            }catch (Exception e){
                throw new RuntimeException(e.getMessage());
            }
        });

        return view;
    }

    public void setTitle(String title){
        getActivity().runOnUiThread(()->{
            plTitle.setText(title);
        });
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void play(){
        if (url != null && !"".equals(url)){
            playerService.playUrl(url);
            getActivity().runOnUiThread(()->{
                plToggle.setCompoundDrawablesWithIntrinsicBounds(icPause, null, null, null);
            });
        }
    }

    public void stop(){
        if (playerService != null){
            playerService.stop();
            getActivity().runOnUiThread(()->{
                plToggle.setCompoundDrawablesWithIntrinsicBounds(icPlay, null, null, null);
            });
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        */
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


}
