package fr.eni.geocard.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.File;
import java.util.ArrayList;

import fr.eni.geocard.R;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.services.LocalisationService;

public class MapFragment extends Fragment implements OnItemGestureListener<OverlayItem> {

    private OnFragmentMapListener mListener;
    private MapView map;
    private Marker userMaker;
    private Drawable icUser;
    private Drawable icMarker;
    private SeekBar mapSeek;
    private TextView mapSeekVal;
    private boolean mapReady;
    private ItemizedOverlayWithFocus<OverlayItem> mOverlay;
    ArrayList<OverlayItem> markers;
    private Handler handler;
    private Runnable runnable;

    @Override
    public boolean onItemSingleTapUp(int index, OverlayItem item) {
        mListener.onMarkerTap(item.getUid());
        return false;
    }

    @Override
    public boolean onItemLongPress(int index, OverlayItem item) {
        return false;
    }

    public void resetMarker() {
        if (mOverlay != null){
            mOverlay.removeAllItems();
        }
    }

    private class AppLocationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(this.getClass().getSimpleName(), "onReceive: received broadcast");
            updateMapPosition(intent);
        }
    }

    private AppLocationReceiver receiver;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mapReady = false;
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        Context ctx = getContext().getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        Configuration.getInstance().setOsmdroidBasePath(new File(Environment.getExternalStorageDirectory(), "osmdroid"));
        Configuration.getInstance().setOsmdroidTileCache(new File(Environment.getExternalStorageDirectory(), "osmdroid/tiles"));
        map = view.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        mapSeek = view.findViewById(R.id.map_seek);
        mapSeekVal = view.findViewById(R.id.map_seek_val);

        mapSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){
                    dragSeekBarEvent(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        icUser = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_person_pin_circle_black_24dp, null);
        icMarker = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_place_marker_24dp, null);

        userMaker = new Marker(map);
        userMaker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        userMaker.setIcon(icUser);


        new Thread(() -> {
            while(LocalisationService.lastLocation == null){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    break;
                }
            }
            if (getActivity() != null){
                GeoPoint defaultPoint = new GeoPoint(LocalisationService.lastLocation.getLatitude(), LocalisationService.lastLocation.getLongitude());
                userMaker.setPosition(defaultPoint);
                IMapController mapController = map.getController();
                getActivity().runOnUiThread(() -> {
                    mapController.setZoom(9d);
                    mapController.setCenter(defaultPoint);
                });
            }
        }).start();

        userMaker.setTitle("Vous êtes ici");
        map.getOverlays().add(userMaker);
        map.setMultiTouchControls(true);
        map.setMinZoomLevel(12d);

        receiver = new AppLocationReceiver();
        markers = new ArrayList<>();

        // markers overlay
        mOverlay = new ItemizedOverlayWithFocus<>(markers, this, getContext());
        mOverlay.setFocusItemsOnTap(false);

        map.getOverlays().add(mOverlay);

        handler = new android.os.Handler();

        if (!mapReady){
            mapReady = true;
            mListener.onMapReady();
        }
        return view;
    }

    private void dragSeekBarEvent(int progress) {
        getActivity().runOnUiThread(() -> {

            double res = (double) progress;
            res = Math.round(res / 10);
            res = res < 1 ? 1 : res;
            mapSeekVal.setText((int)res + " km");
            double finalRes = res;

            if (runnable != null){
                handler.removeCallbacks(runnable);
            }

            runnable = () -> mListener.onChangeMaxDistance(finalRes);
            handler.postDelayed(runnable, 200);

        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentMapListener) {
            mListener = (OnFragmentMapListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        map.onResume();
        IntentFilter filter = new IntentFilter(LocalisationService.REALTIME_POSITION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getContext().registerReceiver(receiver, filter);
    }

    @Override
    public void onPause(){
        super.onPause();
        map.onPause();
        getContext().unregisterReceiver(receiver);
    }

    public void updateMapPosition(Intent intent){
        if (intent.getExtras() != null){
            double latitude = intent.getExtras().getDouble(LocalisationService.EXTRA_LOCATION_LATITUDE);
            double longitude = intent.getExtras().getDouble(LocalisationService.EXTRA_LOCATION_LONGITUDE);
            updateMapPosition(latitude, longitude);
        }
    }

    public void updateMapPosition(){
        if (LocalisationService.lastLocation != null){
            double latitude = LocalisationService.lastLocation.getLatitude();
            double longitude = LocalisationService.lastLocation.getLongitude();
            updateMapPosition(latitude, longitude);
        }
    }

    public void updateMapPosition(double latitude, double longitude){
        if (map != null){
            IMapController mapController = map.getController();
            GeoPoint startPoint = new GeoPoint(latitude, longitude);
            mapController.setCenter(startPoint);
            userMaker.setPosition(startPoint);
            Log.d(this.getClass().getSimpleName(), "updateMapPosition: set new user position");
            mListener.onMapUpdate();
        }
    }

    public void addMarquer(PostCard postCard){
        OverlayItem item =  new OverlayItem(
                postCard.getId(),
                postCard.getTitle(),
                postCard.getMessage(),
                new GeoPoint(postCard.getLatitude(), postCard.getLongitude())
        );
        item.setMarker(icMarker);

        mOverlay.addItem(item);
        markers.add(item);
    }


    public interface OnFragmentMapListener {
        void onMapReady();
        void onMarkerTap(String uuid);
        void onMapUpdate();
        void onChangeMaxDistance(double max);
    }

}
