package fr.eni.geocard.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;
import java.util.UUID;

import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.dal.GeoCardDatabase;
import fr.eni.geocard.dal.PostCardDao;

public class PostCardRepository {
    private PostCardDao postCardDao;
    private LiveData<List<PostCard>> allPostCards;


    public PostCardRepository(Application application) {
        GeoCardDatabase db = GeoCardDatabase.getDatabase(application);
        postCardDao = db.postCardDao();
        allPostCards = postCardDao.selectAll();
    }

    public LiveData<List<PostCard>> getAllPostCards() {
        return allPostCards;
    }

    public LiveData<PostCard> getById(String id) {
        return postCardDao.select(id);
    }

    public LiveData<List<PostCard>> getByUser(int userId) {
        return postCardDao.selectByUser(userId);
    }

    public void insert(PostCard postCard) {
        postCard.setId(UUID.randomUUID().toString());
        new crudAsyncTask(postCardDao, "insert").execute(postCard);
    }

    public void update(PostCard postCard) {
        new crudAsyncTask(postCardDao, "update").execute(postCard);
    }

    public void delete(PostCard postCard) {
        new crudAsyncTask(postCardDao, "delete").execute(postCard);
    }

    private static class crudAsyncTask extends AsyncTask<PostCard, Void, Void> {

        private PostCardDao mAsyncTaskDao;
        private String action;

        crudAsyncTask(PostCardDao dao, String action) {
            mAsyncTaskDao = dao;
            this.action = action;
        }

        @Override
        protected Void doInBackground(final PostCard... params) {
            switch (action) {
                case "insert":
                    mAsyncTaskDao.insert(params[0]);
                    break;
                case "update":
                    mAsyncTaskDao.update(params[0]);
                    break;
                case "delete":
                    mAsyncTaskDao.delete(params[0]);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
