package fr.eni.geocard.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import fr.eni.geocard.bo.Media;
import fr.eni.geocard.dal.GeoCardDatabase;
import fr.eni.geocard.dal.MediaDao;

public class MediaRepository {
    private MediaDao mediaDao;
    private LiveData<List<Media>> allMedias;

    public MediaRepository(Application application) {
        GeoCardDatabase db = GeoCardDatabase.getDatabase(application);
        mediaDao = db.mediaDao();
        allMedias = mediaDao.selectAll();
    }

    public LiveData<List<Media>> getAllMedias() {
        return allMedias;
    }

    public LiveData<Media> getByCardAndType(String cardId, String type) {
        return mediaDao.select(cardId, type);
    }

    public LiveData<List<Media>> getByCard(String cardId) {
        return mediaDao.selectByCard(cardId);
    }

    public void insert(Media media) {
        new crudAsyncTask(mediaDao, "insert").execute(media);
    }

    public void update(Media media) {
        new crudAsyncTask(mediaDao, "update").execute(media);
    }

    public void delete(Media media) {
        new crudAsyncTask(mediaDao, "delete").execute(media);
    }

    private static class crudAsyncTask extends AsyncTask<Media, Void, Void> {

        private MediaDao mAsyncTaskDao;
        private String action;

        crudAsyncTask(MediaDao dao, String action) {
            mAsyncTaskDao = dao;
            this.action = action;
        }

        @Override
        protected Void doInBackground(final Media... params) {
            switch (action) {
                case "insert":
                    mAsyncTaskDao.insert(params[0]);
                    break;
                case "update":
                    mAsyncTaskDao.update(params[0]);
                    break;
                case "delete":
                    mAsyncTaskDao.delete(params[0]);
                    break;
            }
            return null;
        }
    }
}
