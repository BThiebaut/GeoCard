package fr.eni.geocard.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import fr.eni.geocard.bo.User;
import fr.eni.geocard.dal.GeoCardDatabase;
import fr.eni.geocard.dal.UserDao;

public class UserRepository {
    private UserDao userDao;
    private LiveData<List<User>> allUsers;

    public UserRepository(Application application) {
        GeoCardDatabase db = GeoCardDatabase.getDatabase(application);
        userDao = db.userDao();
        allUsers = userDao.selectAll();
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public LiveData<User> getById(int id) {
        return userDao.select(id);
    }

    public User getByTokenId(String tokenId) {
        return userDao.selectByTokenId(tokenId);
    }

    public void insert(User user) {
        new crudAsyncTask(userDao, "insert").execute(user);
    }

    public void update(User user) {
        new crudAsyncTask(userDao, "update").execute(user);
    }

    public void delete(User user) {
        new crudAsyncTask(userDao, "delete").execute(user);
    }

    public void deleteAll() {
        new crudAsyncTask(userDao, "deleteAll").execute();
    }

    private static class crudAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;
        private String action;

        crudAsyncTask(UserDao dao, String action) {
            mAsyncTaskDao = dao;
            this.action = action;
        }

        @Override
        protected Void doInBackground(final User... params) {
            switch (action) {
                case "insert":
                    mAsyncTaskDao.insert(params[0]);
                    break;
                case "update":
                    mAsyncTaskDao.update(params[0]);
                    break;
                case "delete":
                    mAsyncTaskDao.delete(params[0]);
                    break;
                case "deleteAll":
                    mAsyncTaskDao.deleteAll();
                    break;
            }
            return null;
        }
    }
}
