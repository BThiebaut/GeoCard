package fr.eni.geocard.services;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

import fr.eni.geocard.Repository.MediaRepository;
import fr.eni.geocard.Repository.PostCardRepository;
import fr.eni.geocard.bo.Media;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.exceptions.FormException;
import fr.eni.geocard.utils.Utils;

public class PostCardService {


    public static final String VALUE_IMAGE = "image";
    public static final String VALUE_TITRE = "title";
    public static final String VALUE_LATITUDE = "latitude";
    public static final String VALUE_LONGITUDE = "longitude";
    public static final String VALUE_MUSIC = "media_music";

    private static PostCardService instance;
    private PostCardService(){

    }

    public static PostCardService getInstance(){
        if (instance == null){
            instance = new PostCardService();
        }
        return instance;
    }

    public boolean create(Dictionary<String, Object> dataValues, Application application) throws FormException {
        validateDatas(dataValues);
        MediaRepository mr = new MediaRepository(application);

        String image = (String) dataValues.get(VALUE_IMAGE);
        String title = (String) dataValues.get(VALUE_TITRE);
        double latitude = (double) dataValues.get(VALUE_LATITUDE);
        double longitude = (double) dataValues.get(VALUE_LONGITUDE);

        Media music = (Media) dataValues.get(VALUE_MUSIC);

        boolean saved = false;
        PostCard pc = new PostCard();
        pc.setTitle(title);
        pc.setLatitude(latitude);
        pc.setLongitude(longitude);
        pc.setUser(SignInService.appUser);

        PostCardRepository pcr = new PostCardRepository(application);
        pcr.insert(pc);

        Media media = new Media();
        media.setCard(pc);
        media.setType(Media.Type.TYPE_PICTURE.toString());
        media.setUrl(image);
        mr.insert(media);

        if (music != null){
            music.setCard(pc);
            mr.insert(music);
        }

        return saved;
    }

    public void validateDatas(Dictionary<String, Object> dataValues) throws FormException {
        FormException exception = new FormException();

        if (dataValues.get(VALUE_TITRE) == null || dataValues.get(VALUE_TITRE) == ""){
            exception.addMessage("Error field " + VALUE_TITRE);
        }
        if (dataValues.get(VALUE_IMAGE) == null || dataValues.get(VALUE_IMAGE) == ""){
            exception.addMessage("Error field " + VALUE_IMAGE);
        }

        // convertions latitude longitude
        double latitude = -9999d;
        double longitude = -9999d;
        try {
            latitude = (double) dataValues.get(VALUE_LATITUDE);
            longitude = (double) dataValues.get(VALUE_LONGITUDE);
        }catch (Exception e){
            exception.addMessage("Latitude ou longitude incorrect");
        }
        if (latitude == -9999d || longitude == -9999d){
            exception.addMessage("Position incorrect");
        }
        if (exception.hasMessage()){
            throw exception;
        }

    }

    public void getAll(Application application, LifecycleOwner lifecycleOwner, postCardListener listener){
        PostCardRepository pcr = new PostCardRepository(application);
        pcr.getAllPostCards().observe(lifecycleOwner, postCards -> listener.onGetAllResponse(postCards));
    }

    public void getAllByDistance(Application application, final double maxDistance, LifecycleOwner lifecycleOwner, postCardListener listener){
        PostCardRepository pcr = new PostCardRepository(application);
        pcr.getAllPostCards().observe(lifecycleOwner, postCards -> {
            List<PostCard> closePostCard = new ArrayList<>();
            if (LocalisationService.lastLocation != null){
                double currentLat = LocalisationService.lastLocation.getLatitude();
                double currentLng = LocalisationService.lastLocation.getLongitude();

                for(PostCard pc : postCards){
                    double dist = Utils.calculateDistance(currentLat, currentLng, pc.getLatitude(), pc.getLongitude(), Utils.Unit.UNIT_K);
                    if (Double.isNaN(dist) || dist < maxDistance){
                        closePostCard.add(pc);
                    }
                }
            }
            listener.onGetAllResponse(closePostCard);
        });
    }

    public void getById(String id, Application application, LifecycleOwner lifecycleOwner, postCardListener listener) {
        PostCardRepository postCardRepository = new PostCardRepository(application);
        postCardRepository.getById(id).observe(lifecycleOwner, postCard -> {
            listener.onGetOneResponse(postCard);
        });
    }


    public interface postCardListener{
        void onGetAllResponse(List<PostCard> postCards);
        void onGetOneResponse(PostCard postCard);
    }

}
