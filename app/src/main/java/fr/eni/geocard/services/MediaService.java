package fr.eni.geocard.services;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;

import java.util.List;

import fr.eni.geocard.Repository.MediaRepository;
import fr.eni.geocard.bo.Media;

public class MediaService {
    private static MediaService instance;

    private MediaService() {}

    public static MediaService getInstance() {
        if (instance == null){
            instance = new MediaService();
        }
        return instance;
    }

    public void getByCard(String cardId, Application application, LifecycleOwner lifecycleOwner, MediaListener listener) {
        MediaRepository mediaRepository = new MediaRepository(application);

        mediaRepository.getByCard(cardId).observe(lifecycleOwner, media -> {
            listener.onGetAllResponse(media);
        });
    }

    public interface MediaListener{
        void onGetAllResponse(List<Media> media);
    }
}
