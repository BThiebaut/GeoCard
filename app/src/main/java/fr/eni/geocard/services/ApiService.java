package fr.eni.geocard.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fr.eni.geocard.bo.Media;

public class ApiService extends Service {
    private static final String TAG = "ApiService";
    private static final String JAMID = "c3bdeb9a";
    private static final String YOUTUBEKEY = "AIzaSyBbHN3vv4qaMJ3N9sA36N7PM5RuNGQSjiI";
    private Thread apiThread;
    private ApiServiceListeners mListeners;
    private RequestQueue queue;



    private static final String JAMENDO_BASE = "https://api.jamendo.com/v3.0/";
    private static final String YOUTUBE_BASE = "https://www.googleapis.com/youtube/v3/search?";

    public ApiService() {
    }

    public ApiService(ApiServiceListeners listeners, Context context){
        mListeners = listeners;
        queue = Volley.newRequestQueue(context);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private String getJamendoBaseUrl(String option){
        return JAMENDO_BASE + option + "?client_id=" + JAMID + "&format=jsonpretty&limit=5";
    }

    private String getYoutubeBaseUrl(){
        return YOUTUBE_BASE + "key=" + YOUTUBEKEY;
    }

    public void searchFor(String name){
        String searchUrl = getJamendoBaseUrl("tracks") + "&namesearch=" + name;
        Log.d(TAG, "searchFor: searchUrl : " + searchUrl);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, searchUrl, null, response -> {
                        Log.d(TAG, "onResponse: " + response.toString());
                    try {
                        List<Media> musics = new ArrayList<>();
                        JSONArray results = (JSONArray) response.get("results");
                        for(int i = 0; i < results.length(); i++){
                            JSONObject obj = results.getJSONObject(i);
                            String desc = obj.getString("artist_name") + " - " + obj.getString("name");
                            Media media = new Media(Media.Type.TYPE_MUSIC.toString(), obj.getString("audio"), desc);
                            musics.add(media);
                        }
                        mListeners.onSearchResult(musics);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                        error -> Log.e(TAG, "searchFor: error " + error.toString() ));
        queue.add(jsonObjectRequest);
    }

    public void searchVideo(String term){
        String searchUrl = getYoutubeBaseUrl() + "&q=" + term;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, searchUrl, null, response -> {
                    Log.d(TAG, "onResponse: " + response.toString());
                    try {
                        List<Media> videos = new ArrayList<>();

                        JSONArray results = (JSONArray) response.get("items");
                        /*
                        for(int i = 0; i < results.length(); i++){
                            JSONObject obj = results.getJSONObject(i);
                            String desc = obj.getString("artist_name") + " - " + obj.getString("name");
                            Media media = new Media(Media.Type.TYPE_MUSIC.toString(), obj.getString("audio"), desc);
                            musics.add(media);
                        }
                        */
                        mListeners.onVideoResult(videos);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                        error -> Log.e(TAG, "searchVideo: error " + error.toString() ));
        queue.add(jsonObjectRequest);
    }

    public interface ApiServiceListeners{
        void onSearchResult(List<Media> musics);
        void onVideoResult(List<Media> videos);
    }

}
