package fr.eni.geocard.services;

import android.app.Service;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import fr.eni.geocard.MainActivity;
import fr.eni.geocard.Repository.UserRepository;
import fr.eni.geocard.bo.User;
import fr.eni.geocard.dal.GeoCardDatabase;

public class SignInService extends Service {

    private String TAG = "SignInService";
    private static final int CODE_SIGN_IN = 1;
    private SignInListeners listeners;
    public static User appUser;

    public SignInService() {
    }

    public class SignInServiceBinder extends Binder {
        public SignInService getService(SignInListeners listeners) {
            SignInService.this.listeners = listeners;
            return SignInService.this;
        }
    }

    private IBinder binder = new SignInServiceBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        listeners.onSign(account == null);
        return super.onStartCommand(intent, flags, startId);
    }

    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            UserRepository userRepository = new UserRepository(getApplication());

            User userBdd = userRepository.getByTokenId(account.getId());
            if (userBdd == null) {
                User user = googleUserToAppUser(account);
                userRepository.insert(user);
                appUser = user;
            } else {
                appUser = userBdd;
            }
            listeners.onSign(appUser == null);

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            listeners.onSign(true);
        }
    }

    public interface SignInListeners {
        void onSign(boolean hasError);
    }

    private User googleUserToAppUser(GoogleSignInAccount googleUser) {
        User user = new User();
        user.setUsername(googleUser.getDisplayName());
        user.setTokenId(googleUser.getId());
        user.setEmail(googleUser.getEmail());
        return user;
    }
}
