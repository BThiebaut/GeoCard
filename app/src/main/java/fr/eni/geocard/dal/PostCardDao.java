package fr.eni.geocard.dal;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fr.eni.geocard.bo.PostCard;

@Dao
public interface PostCardDao {
    @Insert
    void insert(PostCard... postCards);
    @Update
    void update(PostCard... postCards);
    @Delete
    void delete(PostCard... postCards);
    @Query("SELECT * FROM PostCards WHERE id = :id")
    LiveData<PostCard> select(String id);
    @Query("SELECT * FROM PostCards")
    LiveData<List<PostCard>> selectAll();
    @Query("SELECT * FROM PostCards WHERE user = :userId")
    LiveData<List<PostCard>> selectByUser(int userId);

}
