package fr.eni.geocard.bo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@Entity(tableName = "Users")
public class User implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    @NonNull
    private String username;
    @ColumnInfo(name = "token_id")

    private String tokenId = "";
    @NonNull
    private String email;
    private String password = "";
    private String lastSync = "";

    @Ignore
    public User() {
    }

    public User(int id, String username, String tokenId, String email) {
        this.id = id;
        this.username = username;
        this.tokenId = tokenId;
        this.email = email;
    }

    protected User(Parcel in) {
        id = in.readInt();
        username = in.readString();
        tokenId = in.readString();
        email = in.readString();
        password = in.readString();
        lastSync = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(username);
        dest.writeString(tokenId);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(lastSync);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastSync() {
        return lastSync;
    }

    public void setLastSync(String lastSync) {
        this.lastSync = lastSync;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", lastSync='" + lastSync + '\'' +
                '}';
    }
}
