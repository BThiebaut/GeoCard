package fr.eni.geocard.adapters;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import fr.eni.geocard.Repository.MediaRepository;
import fr.eni.geocard.bo.Media;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.R;

public class PortraitCardAdapter extends ArrayAdapter<PostCard> {
    private Application application;

    public PortraitCardAdapter(Context context, List<PostCard> data, Application application) {
        super(context, R.layout.main_portrait_card, data);
        this.application = application;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.main_portrait_card, parent, false);
        }

        TextView txtTitle = convertView.findViewById(R.id.portrait_card_title);
        ImageView imageView = convertView.findViewById(R.id.portrait_card_image);

        PostCard postCard = getItem(position);
        txtTitle.setText(postCard.getTitle());
        List<Media> mediasCard = postCard.getMedias();
        for (Media media : mediasCard) {
            if (media.getType().equals(Media.Type.TYPE_PICTURE.toString())) {
                Uri uri = Uri.parse(media.getUrl());
                imageView.setImageURI(uri);
                break;
            }
        }
        return convertView;
    }
}
